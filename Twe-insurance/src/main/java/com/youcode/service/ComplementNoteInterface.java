package com.youcode.service;

import java.util.List;
import java.util.Optional;

import com.youcode.entity.ComplementNote;

public interface ComplementNoteInterface {
	
	public ComplementNote addComplementNote(ComplementNote complementnote);
	public List<ComplementNote> getAllComplementsNotes();
	public Optional<ComplementNote> getByIdComplementNote(long idComplement);
	public void deleteComplementtNote(long idComplement);
	public Optional<ComplementNote> findComplementByCodeComplement(String codeComplement);
	public List<ComplementNote> findComplementByNomClass(String nomClass);
	
}
