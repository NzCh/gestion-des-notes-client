package com.youcode.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youcode.entity.ComplementNote;
import com.youcode.repository.ComplementNoteRepository;

@Service
public class ComplementNoteService implements ComplementNoteInterface {

	@Autowired
	ComplementNoteRepository complementrepository;
		
	public ComplementNote addComplementNote(ComplementNote complementnote) {		
		ComplementNote c = complementrepository.save(complementnote);
		return c;
	}
	
	public List<ComplementNote> getAllComplementsNotes() {
		return complementrepository.findAll();
	}
	
	public Optional<ComplementNote> getByIdComplementNote(long idComplement){
		return complementrepository.findById(idComplement);
	}
	
	public void deleteComplementtNote(long idComplement) {
		complementrepository.deleteById(idComplement);
	}
	
	public Optional<ComplementNote> findComplementByCodeComplement(String codeComplement){
		return complementrepository.findComplementByCodeComplement(codeComplement);
	} 
	
	public List<ComplementNote> findComplementByNomClass(String nomClass){
		return complementrepository.findComplementByNomClass(nomClass);
	}
	

}
