package com.youcode.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youcode.entity.ComplementNote;
import com.youcode.entity.ListNote;
import com.youcode.repository.ComplementNoteRepository;
import com.youcode.repository.ListNoteRepository;

@Service
public class ListNoteService implements ListNoteInterface {

	@Autowired
	ListNoteRepository listnoterepository;
	@Autowired
	ComplementNoteRepository complementrepository;
	
	public ListNote addListNote(long idComplement , ListNote listnote) {		
	    Optional<ComplementNote> complementnote = complementrepository.findById(idComplement);
		ComplementNote complementfound = complementnote.get();
		if (complementfound.getIsListNote()) {
			listnote.setComplementnote(complementfound);
		}
		return listnoterepository.save(listnote);		
	}
	
	public List<ListNote> getListesNotes(){
		return listnoterepository.findAll();
	}
	
//	public List<ListNote> findListesNotesOfComplementNote(long id_complement){
//		Optional<ComplementNote> complementnote = complementrepository.findById(id_complement);
//		return complementnote.get().getListnote();		
//	}
	
	public void deleteElementFromListNote(long idListNote){
		listnoterepository.deleteById(idListNote);
	}
	
	public List<ListNote> getListNoteOfComplement(ComplementNote complementnote, String codeComplement){
		return listnoterepository.getListNoteOfComplement(complementnote, codeComplement);
	}
	
	
}
