package com.youcode.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youcode.entity.ComplementNote;
import com.youcode.entity.Note;
import com.youcode.repository.ComplementNoteRepository;
import com.youcode.repository.NoteRepository;

@Service
public class NoteService implements NoteInterface  {

	@Autowired
	NoteRepository noterepository;
	@Autowired ComplementNoteRepository complementrepository;
	
	public Note addNote(long idComplement, Note note) {
		Optional<ComplementNote> complemntnote = complementrepository.findById(idComplement);
		ComplementNote complemntfound = complemntnote.get();
		if (!complemntfound.getIsListNote()) {
			note.setComplementnote(complemntfound);
		}
		return noterepository.save(note);
	}
	
	public void deleteNote(long idNote) {
		noterepository.deleteById(idNote);
	}
	
	public Optional<Note> getNoteById(long idNote) {
		return noterepository.findById(idNote);
	}
	
	public List<Note> getNotesOfComplement(ComplementNote complementnote, String codeComplement){
		return noterepository.getAllNotesOfComplement(complementnote, codeComplement);
	}


}
