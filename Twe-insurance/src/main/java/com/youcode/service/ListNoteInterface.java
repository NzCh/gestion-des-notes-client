package com.youcode.service;

import java.util.List;

import com.youcode.entity.ComplementNote;
import com.youcode.entity.ListNote;

public interface ListNoteInterface {
	
	public ListNote addListNote(long idComplement , ListNote listenote);
	public List<ListNote> getListesNotes();
	//public List<ListNote> findListesNotesOfComplementNote(long id_complement);
	public void deleteElementFromListNote(long idListNote);
	public List<ListNote> getListNoteOfComplement(ComplementNote complementnote, String codeComplement);
	

}
