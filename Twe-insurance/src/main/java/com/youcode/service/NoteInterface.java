package com.youcode.service;

import java.util.List;
import java.util.Optional;

import com.youcode.entity.ComplementNote;
import com.youcode.entity.Note;

public interface NoteInterface {

	public Note addNote(long idComplement, Note note);	
	public void deleteNote(long idNote);	
	public Optional<Note> getNoteById(long idNote);	
	public List<Note> getNotesOfComplement(ComplementNote complementnote, String codeComplement);
	
}
