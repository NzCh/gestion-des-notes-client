package com.youcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TweInsuranceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TweInsuranceApplication.class, args);
	}

}
