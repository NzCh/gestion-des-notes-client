package com.youcode.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "Twe_note")
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "complementnote")
@EnableJpaAuditing
public class Note {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	private long idNote;
	
	@Getter @Setter
	private  UUID uuidNote = UUID.randomUUID();

	@Getter @Setter
	private String note;
	
	@Temporal(TemporalType.DATE)
	@Column(name="created_date", nullable = false, updatable = false)
    @CreatedDate
	@Getter @Setter
	private Date createdDate;
	
	@Getter @Setter
	private long idUser;
	
	@Temporal(TemporalType.DATE)
    @LastModifiedDate
	@Getter @Setter
	private Date modifiedDate;
	
	@Getter @Setter
	private long idUserMaj;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@Getter @Setter
	@JsonIgnore
	@JoinColumn(name="id_complement", nullable = false)
	ComplementNote complementnote;
	 	
}
