package com.youcode.entity;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "Twe_complement_note")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ComplementNote {
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 @Getter @Setter
	 private long idComplement;
	 
	 @Getter @Setter
	 private UUID uuidCompelement = UUID.randomUUID();

	 @Getter @Setter
	 private String codeComplement;

	 @Getter @Setter
	 private String nomClass;

	 @Getter @Setter
	 private Boolean isListNote;
	 
	 @OneToMany(mappedBy = "complementnote")
	 @Getter @Setter
	 //@JsonIgnore
	 private List<Note> note;
	 
	 @OneToMany(mappedBy = "complementnote")
	 @Getter @Setter
	 //@JsonIgnore
	 private List<ListNote> listnote;
    		 	 
}
