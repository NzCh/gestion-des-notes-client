package com.youcode.entity;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "Twe_list_note")
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "complementnote")
public class ListNote {
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 @Getter @Setter
	 private long idListNote;
	 
	 @Getter @Setter
	 private UUID uuidListNote = UUID.randomUUID() ;
	 
	 @Getter @Setter
	 private String text;
	 
	 @Getter @Setter
	 private Boolean actif;
	 
	 @ManyToOne(fetch = FetchType.LAZY)
	 @JoinColumn(name="id_complement", nullable = false)
	 @JsonIgnore
	 @Getter @Setter
	 ComplementNote complementnote;
 
}
