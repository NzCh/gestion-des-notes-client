package com.youcode.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.youcode.entity.ComplementNote;
import com.youcode.entity.ListNote;

@Repository
public interface ListNoteRepository extends JpaRepository<ListNote, Long>{
	
	@Query("SELECT L FROM ListNote AS L WHERE L.complementnote =:complementnote AND L.complementnote.codeComplement =:codeComplement AND L.complementnote.isListNote = 1")
	public List<ListNote> getListNoteOfComplement(@Param("complementnote") ComplementNote complementnote, @Param("codeComplement") String codeComplement);
    
}
