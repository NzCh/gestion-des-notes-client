package com.youcode.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.youcode.entity.ComplementNote;
import com.youcode.entity.Note;

@Repository
public interface NoteRepository  extends JpaRepository<Note, Long>{

	@Query("SELECT N FROM Note AS N WHERE N.complementnote =:complementnote AND N.complementnote.codeComplement =:codeComplement and N.complementnote.isListNote = 0")
	public List<Note> getAllNotesOfComplement(@Param("complementnote") ComplementNote complementnote, @Param("codeComplement") String codeComplement);
    
}
