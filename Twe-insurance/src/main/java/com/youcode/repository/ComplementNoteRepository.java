package com.youcode.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.youcode.entity.ComplementNote;


@Repository
public interface ComplementNoteRepository extends JpaRepository<ComplementNote, Long>{

	@Query("SELECT c FROM ComplementNote c WHERE c.codeComplement  = :codeComplement")
	public Optional<ComplementNote> findComplementByCodeComplement(@Param("codeComplement") String codeComplement);
	
	@Query("SELECT cn FROM ComplementNote cn WHERE cn.nomClass  = :nomClass")
	public List<ComplementNote> findComplementByNomClass(@Param("nomClass") String nomClass);
	
}
