package com.youcode.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.youcode.entity.ComplementNote;
import com.youcode.entity.ListNote;
import com.youcode.repository.ListNoteRepository;
import com.youcode.service.ComplementNoteInterface;
import com.youcode.service.ListNoteInterface;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class ListNoteController {

	@Autowired
	ListNoteInterface listnoteinterface;
	@Autowired
	ListNoteRepository listnoterepository;
	@Autowired
	ComplementNoteInterface complementinterface;
	
	@PostMapping("/listesnnotes/complementsnotes/{idComplement}")
	public ResponseEntity<Object>  addListnoteToComplement(@PathVariable long idComplement,@RequestBody ListNote listnote){		
		listnoteinterface.addListNote(idComplement, listnote);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{idListNote}").buildAndExpand(listnote.getIdListNote()).toUri();
		return ResponseEntity.created(location).build();			
	}
	
	@GetMapping("/listesnnotes")
	public List<ListNote> getAllListeNote(){
		return listnoteinterface.getListesNotes();
	}
	
//	@GetMapping("/listesnnotes/complementsnotes/{id_complement}")
//	public List<ListNote> getByIdComplementListesNotes(@PathVariable long id_complement){
//		return listnoteinterface.findListesNotesOfComplementNote(id_complement); 
//	}
	
	@DeleteMapping("/listesnnotes/{idListNote}")
	public void deleteElementOfListNote(@PathVariable long idListNote){
		listnoteinterface.deleteElementFromListNote(idListNote);
	}
	
	@PutMapping("/listesnnotes/{idListNote}")
	public ResponseEntity<Object> updateListNote(@PathVariable long idListNote, @RequestBody ListNote list) {
		Optional<ListNote> l = listnoterepository.findById(idListNote);
		ListNote listnote = l.get();
		listnote.setText(list.getText());
		listnote.setActif(list.getActif());		
		//listnote.setComplementnote(list.getComplementnote());
		listnoterepository.save(listnote);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{idListNote}").buildAndExpand(listnote.getIdListNote()).toUri();
		return ResponseEntity.created(location).build();	
	}
	
	@GetMapping("/listesnotes/complementsnotes/{idComplement}/codecomplement/{codeComplement}")
	public List<ListNote> getAllListNoteOfComplement(@PathVariable("idComplement") long idComplement, @PathVariable("codeComplement") String codeComplement){
		Optional<ComplementNote> c = complementinterface.getByIdComplementNote(idComplement);
		ComplementNote complementnote = c.get();
		return listnoterepository.getListNoteOfComplement(complementnote, codeComplement);
	}
	

	
	
}
