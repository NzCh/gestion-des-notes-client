package com.youcode.controller;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.youcode.entity.ComplementNote;
import com.youcode.entity.Note;
import com.youcode.repository.NoteRepository;
import com.youcode.service.ComplementNoteInterface;
import com.youcode.service.NoteInterface;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class NoteController {	
	
	  @Autowired 
	  NoteInterface noteinterface;
	  @Autowired
	  NoteRepository noterepository;
	  @Autowired
	  ComplementNoteInterface complementinterface;
	  	  
	  @PostMapping("/notes/complementsnotes/{idComplement}") 
	  public ResponseEntity<Object> addNote(@PathVariable long idComplement, @RequestBody Note note) {		  
		  note.setCreatedDate(new Date());
		  //note.setDate_maj(new Date());
		  noteinterface.addNote(idComplement ,note);
		  URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{idNote}").buildAndExpand(note.getIdNote()).toUri();
		  return ResponseEntity.created(location).build();			  
	  }
	  
	  @DeleteMapping("/notes/{idNote}")
	  public void deleteNote(@PathVariable long idNote) {
		  noteinterface.deleteNote(idNote);
	  }
	  
	  @PutMapping("/notes/{idNote}")
	  public ResponseEntity<Object> getNoteByIdNote(@PathVariable long idNote, @RequestBody Note note){
		  
		  Optional<Note> n = noteinterface.getNoteById(idNote);
		  Note notefound = n.get();
		  //notefound.setComplementnote(note.getComplementnote());
		  notefound.setIdUser(note.getIdUser());
		  notefound.setIdUserMaj(note.getIdUserMaj());
		  notefound.setNote(note.getNote());
		  notefound.setModifiedDate(new Date());
		  noterepository.save(notefound);
		  URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{idNote}").buildAndExpand(note.getIdNote()).toUri();
		  return ResponseEntity.created(location).build();						  
	  }
	  
	  @GetMapping("/notes/complementsnotes/{idComplement}/code_complement/{codeComplement}")
	  public List<Note> getAllNotesOfComplement(@PathVariable("idComplement") long idComplement, @PathVariable("codeComplement") String codeComplement){
		  Optional<ComplementNote> complementfound = complementinterface.getByIdComplementNote(idComplement);
		  ComplementNote complementnote = complementfound.get();
		  System.out.println(complementnote);
		   List<Note> note = noteinterface.getNotesOfComplement(complementnote, codeComplement);
		   System.out.println(note);
		   return note;
	  }
	 

}
