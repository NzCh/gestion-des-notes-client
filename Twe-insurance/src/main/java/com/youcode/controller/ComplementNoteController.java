package com.youcode.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.youcode.entity.ComplementNote;
import com.youcode.service.ComplementNoteInterface;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class ComplementNoteController {

	@Autowired
	ComplementNoteInterface complementinterface;
	
	@PostMapping("/complementsnotes")
	public ResponseEntity<Object> saveComplementNote(@RequestBody ComplementNote complementnote) {
		
		ComplementNote complementnote2 = complementinterface.addComplementNote(complementnote);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{idComplement}").buildAndExpand(complementnote2.getIdComplement()).toUri();
		return ResponseEntity.created(location).build();
		
	}
	
	@GetMapping("/complementsnotes")
	public List<ComplementNote> getAllComplementNote(){
		return complementinterface.getAllComplementsNotes();
	}
	
	@GetMapping("/complementsnotes/{idComplement}")
	public Optional<ComplementNote> getByIdComplementNote(@PathVariable long idComplement){
		Optional<ComplementNote> complementnote = complementinterface.getByIdComplementNote(idComplement);
		return complementnote;
	}
	
	@DeleteMapping("/complementsnotes/{idComplement}")
	public void deleteComplementNote(@PathVariable long idComplement) {
		complementinterface.deleteComplementtNote(idComplement);
	}
	
	@PutMapping("/complementsnotes/{idComplement}")
	public ResponseEntity<Object> updateComplementNote(@PathVariable long idComplement, @RequestBody ComplementNote c) {		
		
		ComplementNote complementnote = complementinterface.getByIdComplementNote(idComplement).get();
		complementnote.setCodeComplement(c.getCodeComplement());
		complementnote.setNomClass(c.getNomClass());
		complementnote.setIsListNote(c.getIsListNote());
		complementinterface.addComplementNote(complementnote);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{idComplement}").buildAndExpand(complementnote.getIdComplement()).toUri();
		return ResponseEntity.created(location).build();
		
	}
	
	//http://localhost:8080/api/complementsnotes/codecomplement?codeComplement=client1
	@GetMapping("/complementsnotes/codecomplement")
	public Optional<ComplementNote> findComplementNoteByCodeComplement(@RequestParam( value = "codeComplement",required = false) String codeComplement){
		return complementinterface.findComplementByCodeComplement(codeComplement);		
	}
	
	@GetMapping("/complementsnotes/nomclass")
	public List<ComplementNote> findComplementNoteByNomClass(@RequestParam( value = "nomClass",required = false) String nomClass){
		return complementinterface.findComplementByNomClass(nomClass);		
	}
	
}
